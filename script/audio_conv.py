#!/env/python
# coding: utf-8

import subprocess
import os

def convert_rep(rep_path, output):
    l = os.listdir(rep_path)
    cpt = 1
    ll = len(l)
    for i in l:
        f = os.path.join(rep_path, i)
        f_out = os.path.join(output, i[:-4] + '.mp3')
        subprocess.call(['avconv', '-i', f, f_out])
        print
        print "DONE %s/%s"%(cpt, ll)
        print
        cpt+=1


def convert_voix(rep_in, rep_out):
    if not os.path.isdir(rep_out):
        os.mkdir(rep_out)
    for v in ['voix', 'voix2', 'voix3']:
        output = os.path.join(rep_out, v)
        if not os.path.isdir(output):
            os.mkdir(output)
        convert_rep(v, output)
