#!/env/python
# coding: utf-8

import json
import csv

path_json_export = '../data/monde_current_topo.json'

path_json = '../data/monde_orig_simple_2_topo.json'
k = 'monde_ctgpop2015_epsg54009_geo'

path_csv = '../data/etat-civil.csv'
path_voix_json_export = '../data/etat-civil-iso.json'
path_iso = '../data/pays_iso'

lst_keys = ['VoixB',
            'dep1iso',
            'dep1total',
            'dep2iso',
            'dep2total',
            'dep3iso',
            'dep3total',
            'nom',
            'pays1iso',
            'pays2iso',
            'pays3iso',
            'prenom',
            'voixA',
            'voixC', 
            'naissance',
            'deces', 
            'annee']

lst_keys_iso=['pays1iso',
            'pays2iso',
            'pays3iso']
    
def get_csv(filename = path_csv):
    l = []
    with open(filename, 'rb') as f:
        c = csv.reader(f)
        entete = c.next()
        cpt = 0
        for i in c:
            d = {'id':cpt}
            cpt +=1
            for i_cle, cle in enumerate(entete):
                d[cle] = i[i_cle]
            l.append(d)
    return l

def get_iso_conversion(filename = path_iso):
    d = {}
    with open(filename, 'r') as f:
        for i in f.readlines():
            s = i.split()
            d[s[0]] = s[1]
    return d

def convert_csv_to_json(filename = path_csv):
    c = get_csv(path_csv)
    d_iso = get_iso_conversion()
    
    c_exp = []
    for i in c:
        dd = {}
        for k in lst_keys:
            if k in lst_keys_iso:
                dd[k] = d_iso[i[k]]
            else:
                dd[k] = i[k]
        c_exp.append(dd)
    return c_exp

class EtatCivil:
    def __init__(self, 
                 path_json = path_json,
                 path_csv = path_csv):
        self.topojson = json.load(open(path_json, 'r'))
        k = self.topojson['objects'].keys()[0]
        self.geometries = self.topojson['objects'][k]['geometries']
        self.geometries_by_id = self.get_g_by_ID6()
        
        self.voix = convert_csv_to_json(path_csv)
        self.clean_naissance()

        self.years = list(set([i['annee'] for i in self.voix]))
        self.years.sort()

        self.add_type()
        self.add_voix()

        self.get_sums_by_years()

        self.clean_45()

    def get_g_by_ID6(self):
        dd = {}
        for i in self.geometries:
            dd[i['properties']['ID6_GK']] = i
        dd['FRA'] = {'properties':{'voix':{}}}

        return dd

    def clean_naissance(self):
        for i in self.voix:
            if i['naissance'].lower() == 'true':
                i['naissance'] = True
            else:
                i['naissance'] = False
                
            if i['deces'].lower() == 'true':
                i['deces'] = True
            else:
                i['deces'] = False

    def clean_45(self):
        a = self.geometries_by_id['FRA45']
        for i, it in a['properties']['voix'].items():
            a['properties']['voix'][i] = max(10, it)
                

    def add_type(self):
        for i in self.geometries:
            if 'FRA' in i['properties']['ID6_GK']:
                i['properties']['type'] = 1
            else:
                i['properties']['type'] = 0

    def add_voix(self):
        d = self.geometries_by_id;
        for i in self.geometries_by_id.values():
            i['properties']['voix'] = {}

        for i, it in enumerate(self.voix):
            if it['dep1iso']: 
                d['FRA'+it['dep1iso']]['properties']['voix'][i] =int(it['dep1total'])
            if it['dep2iso']: 
                d['FRA'+it['dep2iso']]['properties']['voix'][i] =int(it['dep2total'])
            if it['dep3iso']: 
                d['FRA'+it['dep3iso']]['properties']['voix'][i] =int(it['dep3total'])

            if it['pays3iso'] and d.has_key(it['pays3iso']): 
                d[it['pays3iso']]['properties']['voix'][i] = 15
            if it['pays2iso'] and d.has_key(it['pays2iso']): 
                d[it['pays2iso']]['properties']['voix'][i] = 20
            if it['pays1iso'] and d.has_key(it['pays1iso']): 
                d[it['pays1iso']]['properties']['voix'][i] = 25

    def get_by_year(self, year):
        return [i for i in self.voix if i['annee'] == year]


    def get_sums_by_years(self):
        for r in self.geometries:

            r['properties']['years'] = {}
            for y in self.years:
                r['properties']['years'][y] = {}
                r['properties']['years'][y]['naissance'] = 0
                r['properties']['years'][y]['deces'] = 0

        for y in self.years:
            for v in self.get_by_year(y):
                for i in range(1,4):
                    # get depts
                    tot = 'dep%stotal'%i
                    iso = 'dep%siso'%i
                    if v[tot] and v['naissance']:
                        self.geometries_by_id['FRA'+v[iso]]['properties']['years'][y]['naissance'] += int(v[tot])
                    elif v[tot]:
                        self.geometries_by_id['FRA'+v[iso]]['properties']['years'][y]['deces'] += int(v[tot])

                    # gets pays
                    iso = 'pays%siso'%i
                    if v[iso] != 'FRA' and self.geometries_by_id.has_key(v[iso]):
                        if v['naissance']:
                            self.geometries_by_id[v[iso]]['properties']['years'][y]['naissance'] += i
                        else:
                            self.geometries_by_id[v[iso]]['properties']['years'][y]['deces'] += i

        for r in self.geometries:
            r['properties']['years']['all'] = {}
            r['properties']['years']['all']['naissance'] = 0
            r['properties']['years']['all']['deces'] = 0

            for y in self.years:
                r['properties']['years']['all']['naissance'] += r['properties']['years'][y]['naissance']
                r['properties']['years']['all']['deces'] += r['properties']['years'][y]['deces']

    def export_topo(self, filename = path_json_export):
        with open(filename, 'w') as f:
            json.dump(self.topojson, f)

    def export_voix(self, filename = path_voix_json_export):
        with open(filename, 'w') as f:
            json.dump(self.voix, f)

def compare_geom(lst_files):
    a = []
    for i in lst_files:
        a.append({'name':i, 'e':EtatCivil(path_json = '../data/test/%s'%i)})
    return a

def get_fra_arcs(a):
    d = {}
    for i in a.geometries_by_id.keys():
        if 'FRA' in i and i!='FRA':
            d[i] = a.geometries_by_id[i]['arcs']
    return d

def comp_fra_arcs(a, b):
    aa = get_fra_arcs(a)
    bb = get_fra_arcs(b)
    dir_replace = {}
    for i in aa.keys():
        if len(aa[i][0]) != len(bb[i][0]):
            print
            print len(aa[i][0]),len(bb[i][0])
            print aa[i][0]
            print bb[i][0]
        else:
            print
            print len(aa[i][0]),len(bb[i][0])
            print aa[i][0]
            print bb[i][0]
            for j in range(len(aa[i][0])):
                dir_replace[abs(aa[i][0][j])] = abs(bb[i][0][j]) 
    return dir_replace

# g = add_type(g) 
# d = get_g_by_ID6(g)
# d = add_voix(c, d)

# c = convert_csv_to_json()
